# Productivity for Developers

Presentation for IT Lightning Talks: session #16 (CERN, Geneva, 16th June 2018)

To view the slides in the browser, go [here](https://switowski.gitlab.io/productivity-for-developers)

To see the video from the presentation, go [here](http://cds.cern.ch/record/2624991)

PDF version of the slides is available in the *Slides.pdf* file (but slides look way better in the browser)

## Important

This repository will no longer be updated.
